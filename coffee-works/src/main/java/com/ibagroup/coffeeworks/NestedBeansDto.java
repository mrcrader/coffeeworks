package com.ibagroup.coffeeworks;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.ibagroup.coffeeworks.beans.database.dto.Beans;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */
/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
public class NestedBeansDto implements Serializable {

	private Long id;
	private String name;
	
	public NestedBeansDto() {
		
	}
	
	public NestedBeansDto(final Beans entity) {
		if(entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
		}
	}
	
	 public Beans fromDto(Beans entity, EntityManager em) {
	      if (entity == null) {
	         entity = new Beans();
	      }
	      if (this.id != null) {
	         TypedQuery<Beans> findByIdQuery = em
	               .createQuery(
	                     "SELECT DISTINCT b FROM Beans b WHERE b.id = :entityId",
	                     Beans.class);
	         findByIdQuery.setParameter("entityId", this.id);
	         try {
	            entity = findByIdQuery.getSingleResult();
	         }
	         catch (javax.persistence.NoResultException nre) {
	            entity = null;
	         }
	         return entity;
	      }
	      entity.setName(this.name);
	      entity = em.merge(entity);
	      return entity;
	   }

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	 
}
