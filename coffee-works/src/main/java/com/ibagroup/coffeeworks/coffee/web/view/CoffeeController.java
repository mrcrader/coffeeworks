package com.ibagroup.coffeeworks.coffee.web.view;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */
@Model
public class CoffeeController {

	@Inject
	private OnAddCoffeeButtonClick addCoffeeButtonClick;
	
	@Inject
	private RetrieveAllCoffeeOnPageLoad retrieveAllCoffeeOnPageLoad;
	
	/**
	 * <p>Here we use constructor to retrieve and show all coffee on page load</p>
	 */
	public CoffeeController() {
		retrieveAllCoffeeOnPageLoad.retrieveAllCoffeesOrderedById();
	}
	
	public void addCoffeeButton() throws Exception {
		addCoffeeButtonClick.addButton();
	}
	
}
