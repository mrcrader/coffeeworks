package com.ibagroup.coffeeworks.member.database.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ibagroup.coffeeworks.member.database.dto.Member;
import com.ibagroup.coffeeworks.member.web.bean.MemberBean;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
@RequestScoped
public class MemberDaoImpl implements Serializable {

	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	@Inject
	private Event<Member> memberEventSrc;
	
	// https://stackoverflow.com/questions/39397147/java-difference-between-entity-and-dto
	
	/**
	 * <p>Return all coffee if other params are null</p>
	 * @param params
	 * @return
	 */
	public List<MemberBean> retriveMember(MemberSearchParams params) {
		if (params.getId() != null || params.getName() != null || params.getEmail() != null || params.getPhoneNumber() != null || params.getDescription() != null) {
	  	  TypedQuery<Member> query = em.createQuery( "SELECT m FROM Member m WHERE m.id = :id OR m.name = :name OR m.phoneNumber = :phoneNumber OR m.email = :email OR m.description = :description", Member.class);   
		  	if (params.getId() != null)
		  			query.setParameter("id", params.getId());
		  		else query.setParameter("id", null);
			if (params.getName() != null)
					query.setParameter("name", params.getName());
				else query.setParameter("name", null);
			if (params.getPhoneNumber() != null)
					query.setParameter("phoneNumber", params.getPhoneNumber());
				else  query.setParameter("phoneNumber", null);
			if (params.getEmail() != null)
					query.setParameter("email", params.getEmail());
				else query.setParameter("email", null);
			if (params.getDescription() != null)
					query.setParameter("description", params.getDescription());
				else query.setParameter("description", null);
	      final List<Member> searchResults = query.getResultList();
	      final List<MemberBean> results = new ArrayList<MemberBean>();
	      for (Member searchResult : searchResults) {
	      	 MemberBean dto = new MemberBean(searchResult);
	         results.add(dto);
	      }
	      return results;
		} else {
			 TypedQuery<Member> findAllQuery = em.createQuery("SELECT DISTINCT m FROM Member m ORDER BY m.id", Member.class);
		     final List<Member> searchResults = findAllQuery.getResultList();
		     final List<MemberBean> results = new ArrayList<MemberBean>();
		       for (Member searchResult : searchResults) {
		    	   MemberBean dto = new MemberBean(searchResult);
		    	   
		         results.add(dto);
		       }
		  return results;
			}
		}
	
	 public void createNewMember(Member entity) throws Exception{
	      em.persist(entity);
	      memberEventSrc.fire(entity);
	   }
	 
//	 public Response update(Long id, MemberBean dto) {
//	      TypedQuery<Member> findByIdQuery = em.createQuery("SELECT DISTINCT m FROM Member m WHERE m.id = :entityId ORDER BY m.id", Member.class);
//	      findByIdQuery.setParameter("entityId", id);
//	      Member entity;
//	      try {
//	         entity = findByIdQuery.getSingleResult();
//	      }
//	      catch (NoResultException nre) {
//	         entity = null;
//	      }
//	      entity = dto.fromDto(entity, em);
//	      try {
//	         entity = em.merge(entity);
//	      }
//	      catch (OptimisticLockException e) {
//	         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
//	      }
//	      return Response.noContent().build();
//	   }
	 
	 public void deleteMemberById(Long id) {
		  Member entity = em.find(Member.class, id);
	      if (entity == null) {
	         entity = new Member();
	      }
	      em.remove(entity);
	   }
	 
}
