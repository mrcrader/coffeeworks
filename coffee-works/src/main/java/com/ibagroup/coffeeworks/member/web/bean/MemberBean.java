package com.ibagroup.coffeeworks.member.web.bean;

import javax.persistence.EntityManager;

import com.ibagroup.coffeeworks.member.database.dto.Member;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

public class MemberBean {

	private Long id;
	private String name;
	private String email;
	private String phoneNumber;
	private String description;
	
	public MemberBean() {
		
	}
	
	public MemberBean(final Member entity) {
		if (entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.email = entity.getEmail();
			this.phoneNumber = entity.getPhoneNumber();
			this.description = entity.getDescription();
		}
	}
	
	public Member fromDto(Member entity, EntityManager em) {
		if(entity == null) {
			entity = new Member();
		}
			entity.setName(this.name);
			entity.setEmail(this.email);
			entity.setPhoneNumber(this.phoneNumber);
			entity.setDescription(this.description);
			entity = em.merge(entity);
			return entity;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
