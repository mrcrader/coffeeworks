package com.ibagroup.coffeeworks.member.database.dao;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

public class MemberSearchParams {

	private Long id;
	
	private String name;
	
	private String email;
	
	private String phoneNumber;
	
	private String description;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
