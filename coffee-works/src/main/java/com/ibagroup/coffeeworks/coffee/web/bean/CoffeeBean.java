package com.ibagroup.coffeeworks.coffee.web.bean;


import com.ibagroup.coffeeworks.NestedBeansDto;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */
public class CoffeeBean {

	private Long id;
	private String name;
	private Integer stars;
	private String location;
	private String description;
	private NestedBeansDto beans;
	
	public CoffeeBean() {
		
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Integer getStars() {
		return stars;
	}
	
	public String getLocation() {
		return location;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public NestedBeansDto getBeans() {
		return beans;
	}

	public void setBeans(NestedBeansDto beans) {
		this.beans = beans;
	}
	
}
