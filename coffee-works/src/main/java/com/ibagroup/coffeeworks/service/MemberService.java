package com.ibagroup.coffeeworks.service;

import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.ibagroup.coffeeworks.member.web.bean.MemberBean;
import com.ibagroup.coffeeworks.member.web.rest.MemberEndpoint;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class MemberService {
	
	public static String fromEmail = "tibo2013w@gmail.com";
	public static String fromEmailPassword = "NKnnshfhr78";
	public static String toEmailSubject = "coffee.works";
	
	@Inject
	private MemberEndpoint endpoint;

//	public void register(MemberDto dto) throws Exception {
//    	String msg = 	"Hello! "
//				+ "\n\nYou have been successfully registered for coffee.works! \n\nName: " + dto.getName() +
//				"\n\n With best wishes,\n Team IBA Group.";
//    	endpoint.create(dto);
//    	sendEmail(fromEmail, fromEmailPassword, dto.getEmail(), toEmailSubject, msg);  
//    }
    
    public static void sendEmail(final String from,final String password,String to,String sub,String msg)	{  
        //Get properties object    
        Properties props = new Properties();    
        props.put("mail.smtp.host", "smtp.gmail.com");    
        props.put("mail.smtp.socketFactory.port", "465");    
        props.put("mail.smtp.socketFactory.class",    
                  "javax.net.ssl.SSLSocketFactory");    
        props.put("mail.smtp.auth", "true");    
        props.put("mail.smtp.port", "465");    
        //get Session   
        Session session = Session.getInstance(props,    
         new javax.mail.Authenticator() {    
	         protected PasswordAuthentication getPasswordAuthentication() {    
	         return new PasswordAuthentication(from,password);  
	         }    
        });    
        //compose message    
        try {    
	         MimeMessage message = new MimeMessage(session);    
	         message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));    
	         message.setSubject(sub);    
	         message.setText(msg);    
	         //send message  
	         Transport.send(message);    
	         System.out.println("Message sent successfully for email " + to);    
        } catch (MessagingException e) {
        	throw new RuntimeException(e);
      }    
           
  }  

}
