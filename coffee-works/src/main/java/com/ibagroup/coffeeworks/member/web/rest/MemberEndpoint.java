package com.ibagroup.coffeeworks.member.web.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.ibagroup.coffeeworks.member.database.dao.MemberDaoImpl;
import com.ibagroup.coffeeworks.member.database.dao.MemberSearchParams;
import com.ibagroup.coffeeworks.member.database.dto.Member;
import com.ibagroup.coffeeworks.member.web.bean.MemberBean;
import com.ibagroup.coffeeworks.service.MemberService;

/**
 * 
 * @author IBA Group
 * @since 2019
 * 
 * An REST service for {@link Member}
 *
 */

@Path("members")
@Stateless
public class MemberEndpoint {

	@Inject
	MemberDaoImpl dao;
    
	@GET
	@Path("/{id:[0-9][0-9]*}")
    @Produces("application/json")
    public List<MemberBean> findById(@PathParam("id") Long id) {
    	MemberSearchParams params = new MemberSearchParams();
    	params.setId(id);
    	return dao.retriveMember(params);
    }
	
    @GET
    @Path("/{name}")
    @Produces("application/json")
    public List<MemberBean> findByName(@PathParam("name") String name) {
    	MemberSearchParams params = new MemberSearchParams();
    	params.setName(name);
    	return dao.retriveMember(params);
    }
    
    @GET
    @Produces("application/json")
    public List<MemberBean> retriveAllMembers() {
    	MemberSearchParams params = new MemberSearchParams();
    	return dao.retriveMember(params);
    }
    
//    @POST
//    @Consumes("application/json")
//    public Response create(MemberDto dto) {
//    	return dao.create(dto);
//    }
	
}
