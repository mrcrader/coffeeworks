package com.ibagroup.coffeeworks;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.ibagroup.coffeeworks.beans.web.bean.BeansBean;
import com.ibagroup.coffeeworks.coffee.database.dto.Coffee;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
public class NestedCoffeeDto implements Serializable {
	
	private Long id;
	private String name;
	private Integer stars;
	private String location;
	private String description;
	private BeansBean beans;
	
	public NestedCoffeeDto() {
		
	}
	
	public NestedCoffeeDto(final Coffee entity) {
		if(entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.stars = entity.getStars();
			this.location = entity.getLocation();
			this.description = entity.getDescription();
			this.beans = new BeansBean(entity.getBeans());
		}
	}
	
	 public Coffee fromDto(Coffee entity, EntityManager em) {
	      if (entity == null) {
	         entity = new Coffee();
	      }
	      if (this.id != null) {
	         TypedQuery<Coffee> findByIdQuery = em
	               .createQuery(
	                     "SELECT DISTINCT c FROM Coffee c WHERE c.id = :entityId",
	                     Coffee.class);
	         findByIdQuery.setParameter("entityId", this.id);
	         try {
	            entity = findByIdQuery.getSingleResult();
	         }
	         catch (javax.persistence.NoResultException nre) {
	            entity = null;
	         }
	         return entity;
	      }
	      entity.setName(this.name);
	      entity.setStars(this.stars);
	      entity.setLocation(this.location);
	      entity.setDescription(this.description);
	      if (this.beans != null) {
	          entity.setBeans(this.beans.fromDto(entity.getBeans(), em));
	       }
	      entity = em.merge(entity);
	      return entity;
	   }

		public Long getId() {
			return id;
		}
	
		public String getName() {
			return name;
		}
	
		public Integer getStars() {
			return stars;
		}
	
		public String getLocation() {
			return location;
		}
	
		public String getDescription() {
			return description;
		}
	
		public void setId(Long id) {
			this.id = id;
		}
	
		public void setName(String name) {
			this.name = name;
		}
	
		public void setStars(Integer stars) {
			this.stars = stars;
		}
	
		public void setLocation(String location) {
			this.location = location;
		}
	
		public void setDescription(String description) {
			this.description = description;
		}

		public BeansBean getBeans() {
			return beans;
		}

		public void setBeans(BeansBean beans) {
			this.beans = beans;
		}
	 
}
