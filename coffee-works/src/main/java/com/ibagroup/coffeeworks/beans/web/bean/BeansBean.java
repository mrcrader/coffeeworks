package com.ibagroup.coffeeworks.beans.web.bean;

import javax.persistence.EntityManager;

import com.ibagroup.coffeeworks.beans.database.dto.Beans;

public class BeansBean {

	private Long id;
	private String name;
	
	public BeansBean() {
		
	}
	
	public BeansBean(Beans entity) {
		this.id = entity.getId();
		this.name = entity.getName();
	}
	
	public Beans fromDto(Beans entity, EntityManager em) {
		if(entity == null) {
			entity = new Beans();
		}
		entity.setName(this.name);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
